﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tester : MonoBehaviour {

    public KeyCode code;
    public AID.SoundBankNames bank;
    
    // Use this for initialization
    void Start () {
     
    }
	
	// Update is called once per frame
	void Update () {
		
        if(Input.GetKeyDown(code))
        {
            AID.AudioManager.PlayOnce(bank);
        }
	}
}
