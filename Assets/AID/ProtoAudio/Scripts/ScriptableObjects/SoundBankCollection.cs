﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AID
{
    [CreateAssetMenu()]
    public class SoundBankCollection : ScriptableObject
    {
        public SoundBank[] banks;
    }
}