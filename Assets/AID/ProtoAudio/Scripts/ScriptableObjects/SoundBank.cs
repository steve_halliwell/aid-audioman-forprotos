﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AID
{
    [System.Serializable]
    [CreateAssetMenu()]
    public class SoundBank : ScriptableObject
    {
        [HideInInspector]
        [SerializeField]
        public int ID;
        public AudioClip[] clips;

        public AudioClip RandomClip()
        {
            return clips[Random.Range(0, clips.Length)];
        }
    }
}