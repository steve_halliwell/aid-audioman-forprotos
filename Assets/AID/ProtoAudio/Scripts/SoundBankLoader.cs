﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AID
{
    public class SoundBankLoader : MonoBehaviour
    {
        public List<SoundBankCollection> collectionsToLoad = new List<SoundBankCollection>();

        void OnEnable()
        {
            for (int i = 0; i < collectionsToLoad.Count; i++)
            {
                AudioManager.LoadSoundBankCollection(collectionsToLoad[i]);

            }
        }


        void OnDisable()
        {
            for (int i = 0; i < collectionsToLoad.Count; i++)
            {
                AudioManager.UnLoadSoundBankCollection(collectionsToLoad[i]);
            }
        }
    }
}