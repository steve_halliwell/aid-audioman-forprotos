﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace AID
{
    public class AudioManager
    {
        public static List<SoundBank> banks = new List<SoundBank>();

        static public void LoadSoundBankCollection(SoundBankCollection sbc)
        {
            banks.AddRange(sbc.banks);
            banks = banks.Distinct().ToList();//.RemoveAll(null);
            banks.RemoveAll(x => x == null);
        }

        static public void PlayOnce(SoundBankNames ID)
        {
            //find the thing
            var res = banks.Find(x => x.ID == (int)ID);

            //TODO pro mode, default error clip
            if (res == null)
            {
                //no such bank
                Debug.Log("AudioManager cannot find bank of ID; " + ID.ToString());
                return;//bail out
            }

            //choose a clip
            var clipToPlay = res.RandomClip();

            AudioManagerBehaviour.Instance.audioSource.PlayOneShot(clipToPlay);
        }

        internal static void UnLoadSoundBankCollection(SoundBankCollection soundBankCollection)
        {
            foreach (var item in soundBankCollection.banks)
            {
                banks.RemoveAll(x => x == item);
            }
        }
    }

    [RequireComponent(typeof(AudioSource))]
    [DefaultExecutionOrder(-100)]
    [AddComponentMenu("")]
    public class AudioManagerBehaviour : MonoBehaviour
    {
        AudioSource _audioSource;
        public AudioSource audioSource
        {
            get
            {
                if (_audioSource == null)
                    _audioSource = GetComponent<AudioSource>();

                return _audioSource;
            }
        }

        private static AudioManagerBehaviour _inst;
        public static AudioManagerBehaviour Instance
        {
            get
            {
                if(_inst == null)
                {
                    var g = new GameObject("AudioManagerBehaviour");
                    DontDestroyOnLoad(g);
                    _inst = g.AddComponent<AudioManagerBehaviour>();
                }

                return _inst;
            }
        }
    }
}