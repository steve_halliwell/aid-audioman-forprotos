﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;
using System.Linq;

namespace AID
{
    public class SoundBankCreator
    {
        public static string AbsAudioPath
        {
            get
            {
                return Application.dataPath + "/Audio/";
            }
        }

        public static string LocalAudioPath
        {
            get
            {
                return "Assets/Audio/";
            }
        }

        public static string AllBankName
        {
            get
            {
                return "AllSoundBanks";
            }
        }

        [MenuItem("Create/SoundBanks")]
        public static void CreateSoundBanks()
        {
            try
            {
                var allbanksCol = GetAllBankCollection();
                List<SoundBank> allbanks = new List<SoundBank>();

                EditorUtility.DisplayProgressBar("Build Sound Banks", "Finding folders", 0);
                var folders = Directory.GetDirectories(AbsAudioPath, "*", SearchOption.TopDirectoryOnly);

                var sb = new StringBuilder();
                sb.Append(@"namespace AID
{
public enum SoundBankNames
{
");
                //traverse audio folder
                //find all subfolders
                //for each
                for (int i = 0; i < folders.Length; i++)
                {
                    var item = folders[i];
                    //add it to the enum
                    var bankName = Path.GetFileName(item);
                    sb.Append('\t');
                    sb.Append(bankName);
                    sb.Append(@",
");

                    EditorUtility.DisplayProgressBar("Build Sound Banks", "Processing folder: " + bankName, (float)((double)(i)) / folders.Length);
                    //generate soundbank scriptable object
                    allbanks.Add(GenerateSoundBank(item, i));
                }

                sb.Append(@"}
}");

                var enumPath = LocalAudioPath + "SoundBankNames.cs";
                File.WriteAllText(enumPath, sb.ToString());

                AssetDatabase.ImportAsset(enumPath);

                //gather all sound banks
                allbanksCol.banks = allbanks.ToArray();
                EditorUtility.SetDirty(allbanksCol);
            }
            catch (System.Exception)
            {
            }



            EditorUtility.ClearProgressBar();
        }

        static private SoundBank GenerateSoundBank(string directoryName, int id)
        {
            SoundBank soundBankSO = null;

            var fullDirectoryName = Path.GetFullPath(directoryName);

            var bankName = Path.GetFileName(directoryName);

            string[] searchInFolders = { LocalAudioPath + bankName };
            var assets = AssetDatabase.FindAssets("t:AudioClip", searchInFolders);

            List<AudioClip> clips = new List<AudioClip>();
            foreach (var item in assets)
            {
                var path = AssetDatabase.GUIDToAssetPath(item);
                var fullAssetPath = Path.GetFullPath(path);
                if(Path.GetDirectoryName(fullAssetPath) == fullDirectoryName)
                    clips.Add(AssetDatabase.LoadAssetAtPath<AudioClip>(path));
            }

            clips.RemoveAll(x => x == null);


            //if there isn't a scriptableobject then find one
            var results = AssetDatabase.FindAssets(bankName + " t:SoundBank");
            if (results != null && results.Length > 0)
            {
                soundBankSO = AssetDatabase.LoadAssetAtPath<SoundBank>(AssetDatabase.GUIDToAssetPath(results[0]));
            }

            if (soundBankSO == null)
            {
                soundBankSO = SoundBank.CreateInstance<SoundBank>();
                AssetDatabase.CreateAsset(soundBankSO, LocalAudioPath + bankName + ".asset");
            }

            soundBankSO.clips = clips.ToArray();
            soundBankSO.ID = id;
            EditorUtility.SetDirty(soundBankSO);

            return soundBankSO;
        }

        private static SoundBankCollection GetAllBankCollection()
        {
            SoundBankCollection retval = null;
            var res = AssetDatabase.FindAssets(AllBankName + " t:SoundBankCollection");

            if (res != null && res.Length > 0)
            {
                retval = AssetDatabase.LoadAssetAtPath<SoundBankCollection>(AssetDatabase.GUIDToAssetPath(res[0]));
            }

            if (retval == null)
            {
                retval = SoundBankCollection.CreateInstance<SoundBankCollection>();
                AssetDatabase.CreateAsset(retval, LocalAudioPath + AllBankName + ".asset");
            }

            return retval;
        }
    }

}